####Projede Java Spring Boot,hibernate kullanılmıştır. 
####Gerekli use case senaryoları için apiler ve servis katmanları yazılmış
####gerekli işlemler ve crud metodları yazılmıştır.
####Entity classları oluşturulmuştur ve aralarındaki ilişki şu şekildedir.

###Offer Class
#####-OfferId
#####-offerTanim;
#####-fiyat;
#####-vehicle_Id;(foreign key referencing Vehicle class)

###AdditionalOffer Class
####-additionalOfferId
####-extraOzellik
####-extraFiyat
####-vehicle_Id;(foreign key referencing Vehicle class)
####-offerId(foreign key referencing Offer class)

###Sale Class
####-saleId
####-vehicle_Id(foreign key referencing Vehicle class)
####-userId(foreign key referencing UserClass)
####-sale_date;

###User Class
####-userId
####-kullaniciAd;
####-kullaniciSifre;
####-kullaniciOlusturmaTrh;
####-kullaniciEmail;

###UserRoles Class(admin kullanıcı ile normal kullanıcıyı ayırt etmek için eklendi)
####-userRolesId
####-userId(foreign key referencing User class)
####-kullaniciRolAd
####-kullaniciRolKod

###Vehicle Class(araç özelliklerinin bulunduğu sınıf)
####-vehicle_Id
####-aracAd
####-aracRengi
####-model
####-kasaTipi
####-modelYili

###UNIT TESTLER
####Her durum senaryosu için unit test yazılmamış fakat 
####OfferRepositoryTest sınıfında crud testleri yazılmıştır. 
####SaleControllerTest sınıfında FindMostPurchasedProduct ve SeeCustomerProduct metodlarının testleri yazılmıştır.