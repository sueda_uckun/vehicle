package controller;

import entity.Offer;
import entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import service.UserService;

import java.util.List;

@Controller
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/userSave")
    public ResponseEntity<User> save(@RequestBody User user){
        User savedUser = userService.save(user);
        return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
    }

    @PutMapping("/userUpdate/{id}")
    public ResponseEntity<User> updateOffer(@RequestBody User user){
        User updatedUser = userService.updateUser(user);
        return new ResponseEntity<User>(updatedUser, HttpStatus.OK);
    }

    @DeleteMapping("/userDelete/{id}")
    public ResponseEntity<Void> deleteOfferById(@PathVariable(name = "id") Long userId){
        userService.deleteUserById(userId);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }
    @GetMapping("/usersAll")
    public ResponseEntity<List<User>> getAllOffer(){
        List<User> allUser = userService.getAllUser();
        return new ResponseEntity<List<User>>(allUser, HttpStatus.OK);
    }

}
