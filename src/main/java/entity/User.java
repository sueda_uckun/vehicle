package entity;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name="User")
public class User{

    @Id
    @SequenceGenerator(initialValue = 1, name = "idGenerator", sequenceName = "SEQ_User")
    @GeneratedValue(generator = "idGenerator", strategy = GenerationType.AUTO)
    @Column(name = "USER_ID", unique = true, nullable = false, columnDefinition = "long")
    private Long userId;

    @Column(name ="kullaniciAd",columnDefinition = "varchar(50)")
    private String kullaniciAd;

    @Column(name = "KULLANICI_SIFRE", length = 64, columnDefinition = "varchar(64)")
    private String kullaniciSifre;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "KULLANICI_OLUSTURMA_TRH", length = 16, columnDefinition = "smalldatetime")
    private Date kullaniciOlusturmaTrh;

    @Column(name = "KULLANICI_EMAIL", length = 50, columnDefinition = "varchar(50)")
    private String kullaniciEmail;

    public User() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getKullaniciAd() {
        return kullaniciAd;
    }

    public void setKullaniciAd(String kullaniciAd) {
        this.kullaniciAd = kullaniciAd;
    }

    public String getKullaniciSifre() {
        return kullaniciSifre;
    }

    public void setKullaniciSifre(String kullaniciSifre) {
        this.kullaniciSifre = kullaniciSifre;
    }

    public Date getKullaniciOlusturmaTrh() {
        return kullaniciOlusturmaTrh;
    }

    public void setKullaniciOlusturmaTrh(Date kullaniciOlusturmaTrh) {
        this.kullaniciOlusturmaTrh = kullaniciOlusturmaTrh;
    }

    public String getKullaniciEmail() {
        return kullaniciEmail;
    }

    public void setKullaniciEmail(String kullaniciEmail) {
        this.kullaniciEmail = kullaniciEmail;
    }
}
