package entity;

import jakarta.persistence.*;

@Entity
@Table(name="Vehicle")
public class Vehicle {

    @Id
    @SequenceGenerator(initialValue = 1, name = "idGenerator", sequenceName = "SEQ_Vehicle")
    @GeneratedValue(generator = "idGenerator", strategy = GenerationType.AUTO)
    @Column(name = "VehicleId", unique = true, nullable = false, columnDefinition = "long")
    private Long vehicle_Id;

    @Column(name ="AracAd",columnDefinition = "varchar(50)")
    private String aracAd;

    @Column(name ="AracRengi",columnDefinition = "varchar(20)")
    private String aracRengi;

    @Column(name ="Model",columnDefinition = "varchar(50)")
    private String model;

    @Column(name ="KasaTipi",columnDefinition = "varchar(20)")
    private String kasaTipi;

    @Column(name ="AracRengi",columnDefinition = "int")
    private Integer modelYili;

    public Vehicle() {
    }

    public Long getVehicle_Id() {
        return vehicle_Id;
    }

    public void setVehicle_Id(Long vehicle_Id) {
        this.vehicle_Id = vehicle_Id;
    }

    public String getAracAd() {
        return aracAd;
    }

    public void setAracAd(String aracAd) {
        this.aracAd = aracAd;
    }

    public String getAracRengi() {
        return aracRengi;
    }

    public void setAracRengi(String aracRengi) {
        this.aracRengi = aracRengi;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getKasaTipi() {
        return kasaTipi;
    }

    public void setKasaTipi(String kasaTipi) {
        this.kasaTipi = kasaTipi;
    }

    public Integer getModelYili() {
        return modelYili;
    }

    public void setModelYili(Integer modelYili) {
        this.modelYili = modelYili;
    }
}
