package repo;

import entity.AdditionalOffer;
import entity.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdditionalOfferRepository extends JpaRepository<AdditionalOffer,Long> {
}
