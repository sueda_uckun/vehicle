package repo;

import entity.User;
import entity.UserRoles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRoles,Long> {
    List<UserRoles> findAllById(Long userId);
}
