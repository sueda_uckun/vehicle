package service;

import entity.UserRoles;
import entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repo.SaleRepository;

import java.util.List;

@Service
public class SaleService {
    @Autowired
    SaleRepository saleRepository;
    @Autowired
    UserRolesService userRolesService;

    /*Admin kullanıcısı en fazla satılan ürünü görebilir*/
    public List<Vehicle> findMostPurchasedVehicles(Long userId) {
        if (!isAdminUser(userId)) {
            return saleRepository.findMostPurchasedVehicles();
        }
        else return null;
    }
    /* Admin kullanacısı müşteri ürününü görebilir*/
    public List<Vehicle> seeCustomerProduct(Long userId) {
        if (!isAdminUser(userId)) {
            return saleRepository.seeCustomerProduct(userId);
        }
        else return null;
    }

    public Boolean isAdminUser(Long userId){
         List<UserRoles> rol=userRolesService.getUserRole(userId);
         if(rol.get(0).getKullaniciRolKod().equals("ADMIN")){
             return true;
         }
         else return false;
    }
}
