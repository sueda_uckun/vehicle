package com.example.vehicle;

import controller.SaleController;
import entity.Vehicle;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import service.SaleService;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

public class SaleControllerTest {
    @InjectMocks
    private SaleController saleController;

    @Mock
    private SaleService saleService;

    @Test
    public void testFindMostPurchasedVehicles() {
        Long userId = 1L;
        List<Vehicle> expectedVehicles = new ArrayList<>();

        Mockito.when(saleService.findMostPurchasedVehicles(userId)).thenReturn(expectedVehicles);

        List<Vehicle> response = saleController.findMostPurchasedVehicles(userId);

        assertThat(response, Matchers.equalTo(expectedVehicles));
    }

    @Test
    public void testSeeCustomerProduct() {
        Long userId = 1L;
        List<Vehicle> expectedVehicles = new ArrayList<>();

        Mockito.when(saleService.seeCustomerProduct(userId)).thenReturn(expectedVehicles);

        List<Vehicle> response = saleController.seeCustomerProduct(userId);

        assertThat(response, Matchers.equalTo(expectedVehicles));

    }
}
